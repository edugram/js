// contants
var EMAIL_REGEX = "/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i";

// elements
var inputMessage = $('.input-message');

// name space
var app = {};
app.validate = {
  isNull: function(element) {
    if (element.val() === null || element.val() === "") return true;
    return false;
  },
  isEmail: function(element) {
    if (element.val().toString().match(EMAIL_REGEX)) return true;
    return false;
  },
  isPassSame: function(el1, el2) {
    if (el1.val().toString() === el2.val().toString()) return true;
    return false;
  }
};

app.messages = {
  whenError: function(element, message) {
    if (element.next('.input-message').length === 0)
      element.after('<small class="input-message error">' + message + '</small>');
  }
};

app.events = {
  onSubmit: function(formElement, cb) {
    formElement.on('submit', function(e) {
      cb(e);
    });
  },
  onKeyUp: function(element, inputType) {

  },
  onClick: function(elements, cb) {
    elements.on('click', function(e) {
      cb(e);
    });
  }
};

app.preview = function(input, imgPreviewElement) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      imgPreviewElement.attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
  }
};

app.request = function(options, cb) {
  console.log("OPTIONS");
  console.log(options);
  // ajax request here
  // sementara load progress bar dulu aja
  options.loading.fadeIn('fast');
  setTimeout(function() {
    options.loading.fadeOut('slow');
  }, 5000);

  cb('done');
};

app.files = {
  limitSize: function(element, size) {

  },
  fileType: function(element) {

  }
};
