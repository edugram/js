//form submit
var elements = {
  formRegistrasi: $('form[name="formRegistrasi"]'),
  inputNama: $('input[name="nama"]'),
  inputEmail: $('input[name="email"]'),
  inputPassword: $('input[name="password"]'),
  inputKonfirmasiPassword: $('input[name="konfirmasiPassword"]'),
  inputFilePhoto: $('input[name="filePhoto"]'),
  loading: $('.progress'),
  buttonReg: $('button[name="buttonReg"]'),
  previewGambar: $('#previewGambar')
};

// image preview
elements.inputFilePhoto.on('change', function(){
  app.preview(this, elements.previewGambar);
});

app.events.onClick(elements.buttonReg, function(e) {

  //cek isNull nama
  if (app.validate.isNull(elements.inputNama)) app.messages.whenError(elements.inputNama, 'Nama Masih Kosong');
  if (app.validate.isNull(elements.inputEmail)) app.messages.whenError(elements.inputEmail, 'Email Masih Kosong');
  if (app.validate.isNull(elements.inputPassword)) app.messages.whenError(elements.inputPassword, 'Password Masih Kosong');
  if (app.validate.isNull(elements.inputKonfirmasiPassword)) app.messages.whenError(elements.inputKonfirmasiPassword, 'Konfirmasi Password Masih Kosong');

  //cek isPassSame
  if (!app.validate.isPassSame(elements.inputKonfirmasiPassword, elements.inputPassword)) app.message.whenError(elements.inputKonfirmasiPassword, 'Konfirmasi Password Tidak Sama');

  app.request({
    loading: elements.loading
  }, function(r) {
    console.log(r);
    console.log("LOADING SELESAI");
  });
});

// app.events.onSubmit(elements.formRegistrasi, function(e) {
//   app.request({
//     loading: elements.loading
//   }, function(r) {
//     console.log(r);
//     console.log("LOADING SELESAI");
//   });
// });
