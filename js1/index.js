function loadDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      document.getElementById("loadingText").innerHTML = xhttp.responseText;
    }
  };
  xhttp.open("GET", "https://www.google.co.id", true);
  xhttp.send();
}

var buttonDaftar = document.getElementById('buttonDaftar');

buttonDaftar.addEventListener('click', function(e) {
  document.getElementById("loadingText").innerHTML = "Silahkan Tunggu .....";
  loadDoc();
})
